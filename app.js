const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const fetch = require('node-fetch')
const cors = require('cors');
const path = require('path')


//Port from environment variable or default - 4001
const port = process.env.PORT || 4001;

// not a good idea 
const clientId = '221918101508-ce3tkql6s6kc810mnp7crej1hr1svi4l.apps.googleusercontent.com';
const clientSecret = '43i6i7w8WP0yhdeYbYYQ8Ijj';

//Setting up express and adding socketIo middleware
const app = express();
const server = http.createServer(app);
const io = socketIo(server);

app.use(cors())


app.use(express.static(path.join(__dirname, 'build')))

app.get('/oauth', handleOAuth2)
async function handleOAuth2( req, res) {
    const tokenResponse = await fetch('https://www.googleapis.com/oauth2/v4/token',
    {
        method: 'POST',
        body: JSON.stringify({
          code: req.query.code,
          client_id: clientId,
          client_secret: clientSecret,
          redirect_uri: 'https://oslack-app.herokuapp.com/oauth',
          grant_type: 'authorization_code'
        })
      })
      const token = await tokenResponse.json()
      const userInfo = await getUserInfo(token.access_token)
      res.redirect(`https://oslack-app.herokuapp.com/dashboard?${Object.keys(userInfo).map(key => `${key}=${encodeURIComponent(userInfo[key])}`).join('&')}`)
}

async function getUserInfo(accessToken) {
    const response = await fetch(
      `https://www.googleapis.com/oauth2/v1/userinfo?access_token=${accessToken}`,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }
    )
    const json = await response.json()
    return json
  }

  let users = [];

//Setting up a socket with the namespace "connection" for new sockets
io.on("connection", socket => {
    console.log("New client connected" + socket.id);

    socket.on("join", data => {

      let obj = data
      if (users.indexOf(data) < 0) {
        users.push(data)

        io.sockets.emit('addUser', data)
      } 
      io.sockets.emit('addUser', users)
    })

    socket.on("message", (data)=>{
      
       io.sockets.emit("message", data);
    });
    socket.on("addUser", (data) => {
      
     io.sockets.emit("message", data);
  });


    //A special namespace "disconnect" for when a client disconnects
    socket.on("disconnect", (data) => {
        io.sockets.emit('disconnect', data)
    })
});

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

server.listen(port, () => console.log(`Listening on port ${port}`));